# Docker (-compose)

Docker is required to run the application.

[https://www.docker.com/get-started](https://www.docker.com/get-started)

## Start services
Command to run services:

<code>docker-compose up --force-recreate --build</code>

Docker will create a container for the API, a container for MySQL and a network.

API will be available at: http://localhost:8080/

Open it in the browser will open the SwaggerUI (browser client for development)

## Stop services
Command to stop all services:

<code>docker-compose down</code>

### Stop all docker instances
<code>docker stop $(docker ps -a -q)</code>

### Remove all containers
<code>docker container rm $(docker ps -a -q)</code>

### View active docker items
<code>docker container ls -all && docker image ls && docker network ls</code>

# Authorization

Authorization is required to execute following methods:

## GetByIsin

- URL: /api/company/isin/{isin}
- Required role: 'GetByIsin'

## GetById

- URL: /api/company/{companyId}
- Required role: 'GetById'

## How to generate a JWT

You can use this online tool to generate the required JWT: http://jwtbuilder.jamiekurtz.com/

Standard JWT claims:

- Issuer: 'http://www.supercompany.com' (as per appsettings file)
- Issued At: (now)
- Expiration: (any date)
- Audience: 'http://www.superconsumer.com' (as per appsettings file)
- Subject: (any string)

Additional claims (add as necessary):

- role: 'GetByIsin' (to get access to GetByIsin endpoint)
- role: 'GetById' (to get access to GetById endpoint)

Signed JWT:

- key: 'WPTP5mr2dd55oL3mMMBoxEBmiRJ5sc2D' (as per appsettings file)
- hs256

Then hit 'Create signed JWT'

The generated token can be used in the SwaggerUI: 'Bearer {token}'
