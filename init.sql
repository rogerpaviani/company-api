﻿USE companydb
;
CREATE TABLE Company (
	CompanyId BINARY(16) PRIMARY KEY,
	Name VARCHAR(100),
	Exchange VARCHAR(50),
	Ticker VARCHAR(5),
	Isin CHAR(12),
	Website VARCHAR(100)
)
;
INSERT INTO Company VALUES (
	unhex(replace(uuid(),'-','')),
	'Apple Inc.',
	'NASDAQ',
	'AAPL',
	'US0378331005',
	'http://www.apple.com'
)
;
INSERT INTO Company VALUES (
	unhex(replace(uuid(),'-','')),
	'British Airways Plc',
	'Pink Sheets',
	'BAIRY',
	'US1104193065',
	null
)
;
INSERT INTO Company VALUES (
	unhex(replace(uuid(),'-','')),
	'Heineken NV',
	'Euronext Amsterdam',
	'HEIA',
	'NL0000009165',
	null
)
;
INSERT INTO Company VALUES (
	unhex(replace(uuid(),'-','')),
	'Panasonic Corp',
	'Tokyo Stock Exchange',
	'6752',
	'JP3866800000',
	'http://www.panasonic.co.jp'
)
;
INSERT INTO Company VALUES (
	unhex(replace(uuid(),'-','')),
	'Porsche Automobil',
	'Deutsche Börse',
	'PAH3',
	'DE000PAH0038',
	'https://www.porsche.com/'
)
;
