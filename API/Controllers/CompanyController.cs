﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model.Company;
using System;

namespace API.Controllers
{
    [ApiController]
    [Route("api/company")]
    [Produces("application/json")]
    public class CompanyController : ControllerBase
    {
        readonly ICompanyRepository _repository;

        public CompanyController(ICompanyRepository companyRepository)
        {
            _repository = companyRepository;
        }

        [HttpGet("{companyId}")]
        [Authorize(Roles = "GetById")]
        [ProducesResponseType(typeof(Company), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetById(Guid companyId)
        {
            var company = _repository.SelectById(companyId);
            if (company == null)
            {
                return NotFound();
            }
            return Ok(company);
        }

        [HttpGet("isin/{isin}")]
        [Authorize(Roles = "GetByIsin")]
        [ProducesResponseType(typeof(Company), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetByIsin(string isin)
        {
            var company = _repository.SelectByIsin(isin);
            if (company == null)
            {
                return NotFound();
            }
            return Ok(company);
        }

        [HttpGet]
        [ProducesResponseType(typeof(Company[]), StatusCodes.Status200OK)]
        public IActionResult GetAll()
        {
            var result = _repository.SelectAll();
            return Ok(result);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Company), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Post(Company company)
        {
            if (company == null)
            {
                return BadRequest();
            }
            var newCompany = _repository.Insert(company);
            return CreatedAtAction(nameof(GetById), new { companyId = newCompany.CompanyId }, newCompany);
        }

        [HttpPut("{companyId}")]
        [ProducesResponseType(typeof(Company), StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Put(Guid companyId, Company company)
        {
            if (company == null)
            {
                return BadRequest();
            }
            var updatedCompany = _repository.Update(companyId, company);
            return AcceptedAtAction(nameof(GetById), new { companyId = updatedCompany.CompanyId }, updatedCompany);
        }
    }
}