﻿using System;
using System.Linq;

namespace Model.Company
{
    public class CompanyRepository : ICompanyRepository
    {
        readonly ApplicationDbContext _dbContext;

        public CompanyRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Company[] SelectAll()
        {
            return _dbContext.Company.ToArray();
        }

        public Company SelectById(Guid companyId)
        {
            return _dbContext.Company.Find(companyId);
        }

        public Company SelectByIsin(string isin)
        {
            return _dbContext.Company.FirstOrDefault(a => a.Isin == isin);
        }

        public Company Insert(Company company)
        {
            _dbContext.Company.Add(company);
            _dbContext.SaveChanges();
            return company;
        }

        public Company Update(Guid companyId, Company company)
        {
            var local = _dbContext
                .Set<Company>().Local
                .FirstOrDefault(a => a.CompanyId.Equals(companyId));
            if (local != null)
            {
                _dbContext.Entry(local).State = Microsoft.EntityFrameworkCore.EntityState.Detached;
            }
            company.CompanyId = companyId;
            _dbContext.Company.Update(company);
            _dbContext.SaveChanges();
            return company;
        }
    }
}
