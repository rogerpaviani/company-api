﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Model.Company
{
    public class Company
    {
        [Key]
        public Guid CompanyId { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MaxLength(50)]
        public string Exchange { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MinLength(4), MaxLength(5)]
        public string Ticker { get; set; }

        [Required]
        [MinLength(12), MaxLength(12)]
        [RegularExpression(@"^[a-zA-Z]{2}\+[a-zA-Z0-9]{10}\+$", ErrorMessage = "Invalid ISIN format")]
        public string Isin { get; set; }

        [Url]
        public string Website { get; set; }
    }
}
