﻿using System;

namespace Model.Company
{
    public interface ICompanyRepository
    {
        Company[] SelectAll();
        Company SelectById(Guid companyId);
        Company SelectByIsin(string isin);
        Company Insert(Company company);
        Company Update(Guid companyId, Company company);
    }
}
