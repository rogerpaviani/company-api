﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Model.Company;

namespace Model
{
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public static class StartupExtensions
    {
        public static IServiceCollection ConfigureServicesModel(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>((sp, optionsBuilder) =>
            {
                optionsBuilder.UseMySQL(configuration.GetConnectionString("CompanyDB"));
                optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            });
            services.AddScoped<ICompanyRepository, CompanyRepository>();
            return services;
        }
    }
}