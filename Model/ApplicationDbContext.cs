﻿using Microsoft.EntityFrameworkCore;

namespace Model
{
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Company.Company> Company { get; set; }

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Company.Company>()
                .HasIndex(i => i.Isin)
                .IsUnique();
        }
    }
}