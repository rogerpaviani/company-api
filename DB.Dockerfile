FROM mysql:5.7.18

ENV MYSQL_DATABASE companydb
ENV MYSQL_ALLOW_EMPTY_PASSWORD yes

COPY ./init.sql /docker-entrypoint-initdb.d/
