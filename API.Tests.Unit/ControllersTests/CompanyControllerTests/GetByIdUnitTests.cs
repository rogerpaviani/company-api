using System;
using API.Controllers;
using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Model.Company;
using Moq;
using Xunit;

namespace API.Tests.Unit.ControllersTests.CompanyControllerTests
{
    public class GetByIdUnitTests
    {
        private readonly Fixture _fixture;
        private readonly Mock<ICompanyRepository> _mockRepository;
        private readonly CompanyController _controller;

        public GetByIdUnitTests()
        {
            _fixture = new AutoFixture.Fixture();
            _mockRepository = new Mock<ICompanyRepository>();
            _controller = new CompanyController(_mockRepository.Object);
        }

        [Fact]
        public void GivenGetById_WhenDbIsEmpty_ThenReturnNotFound()
        {
            //arrange
            Guid companyId = Guid.NewGuid();

            //act
            var result = _controller.GetById(companyId);

            //assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void GivenGetById_WhenCompanyDoesntExist_ThenReturnNotFound()
        {
            //arrange
            Guid searchCompanyId = Guid.NewGuid();
            var existingCompany = _fixture.Create<Company>();

            _mockRepository
                .Setup(a => a.SelectById(It.Is<Guid>(b => b.CompareTo(existingCompany.CompanyId) == 0)))
                .Returns(existingCompany);

            //act
            var result = _controller.GetById(searchCompanyId);

            //assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void GivenGetById_WhenCompanyExists_ThenReturnOk()
        {
            //arrange
            var existingCompany = _fixture.Create<Company>();

            _mockRepository
                .Setup(a => a.SelectById(It.Is<Guid>(b => b.CompareTo(existingCompany.CompanyId) == 0)))
                .Returns(existingCompany);

            //act
            var result = _controller.GetById(existingCompany.CompanyId);

            //assert
            Assert.IsType<OkObjectResult>(result);
        }
    }
}
