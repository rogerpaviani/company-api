using System.Linq;
using API.Controllers;
using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Model.Company;
using Moq;
using Xunit;

namespace API.Tests.Unit.ControllersTests.CompanyControllerTests
{
    public class GetAllUnitTests
    {
        private readonly Fixture _fixture;
        private readonly Mock<ICompanyRepository> _mockRepository;
        private readonly CompanyController _controller;

        public GetAllUnitTests()
        {
            _fixture = new AutoFixture.Fixture();
            _mockRepository = new Mock<ICompanyRepository>();
            _controller = new CompanyController(_mockRepository.Object);
        }

        [Fact]
        public void GivenGetAll_WhenDbIsEmpty_ThenReturnEmptyList()
        {
            //arrange
            //empty

            //act
            var result = _controller.GetAll();

            //assert
            Assert.IsType<OkObjectResult>(result);
            var okResult = result as OkObjectResult;
            Assert.IsType<Company[]>(okResult.Value);
            var resultCompanies = okResult.Value as Company[];
            Assert.Empty(resultCompanies);
        }

        [Fact]
        public void GivenGetAll_WhenDbHasCompanies_ThenReturnCompanies()
        {
            //arrange
            var companies = _fixture.CreateMany<Company>().ToArray();

            _mockRepository
                .Setup(a => a.SelectAll())
                .Returns(companies);

            //act
            var result = _controller.GetAll();

            //assert
            Assert.IsType<OkObjectResult>(result);
            var okResult = result as OkObjectResult;
            Assert.IsType<Company[]>(okResult.Value);
            var resultCompanies = okResult.Value as Company[];
            Assert.NotEmpty(resultCompanies);
        }
    }
}
