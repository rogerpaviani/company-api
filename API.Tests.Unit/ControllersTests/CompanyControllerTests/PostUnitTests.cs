using API.Controllers;
using AutoFixture;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Model.Company;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace API.Tests.Unit.ControllersTests.CompanyControllerTests
{
    public class PostUnitTests : IClassFixture<WebApplicationFactory<Startup>>, IDisposable
    {
        private readonly Fixture _fixture;
        private readonly Mock<ICompanyRepository> _mockRepository;
        private readonly CompanyController _controller;
        private readonly ITestOutputHelper _outputHelper;
        private readonly TestServer _testServer;

        public PostUnitTests(ITestOutputHelper outputHelper)
        {
            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(new Dictionary<string, string>
                {
                    {"AppSettings:JWT:SecretKey", "x"},
                    {"AppSettings:JWT:Issuer", "x"},
                    {"AppSettings:JWT:Audience", "x"},
                    {"ConnectionStrings:CompanyDB", "server=a;database=b;userid=c;pwd=;port=3306;"}
                })
                .Build();
            var builder = new WebHostBuilder()
                .UseEnvironment("Development")
                .UseConfiguration(configuration)
                .UseStartup<Startup>();
            _testServer = new TestServer(builder);

            _fixture = new AutoFixture.Fixture();
            _mockRepository = new Mock<ICompanyRepository>();
            _controller = new CompanyController(_mockRepository.Object);

            _outputHelper = outputHelper;
        }

        [Fact]
        public void GivenPost_WhenCompanyIsValid_ThenReturnCreated()
        {
            //arrange
            var company = _fixture.Create<Company>();
            _mockRepository
                .Setup(a => a.Insert(It.IsAny<Company>()))
                .Returns(company)
                .Verifiable();

            //act
            var result = _controller.Post(company);

            //assert
            _mockRepository
                .Verify(a => a.Insert(It.Is<Company>(c => c.Name == company.Name)), Times.Once());
            Assert.IsType<CreatedAtActionResult>(result);
            var createdResult = result as CreatedAtActionResult;
            Assert.NotNull(createdResult);
            Assert.IsType<Company>(createdResult.Value);
            var resultCompany = createdResult.Value as Company;
            Assert.NotNull(resultCompany);
            Assert.Equal(company.CompanyId, resultCompany.CompanyId);
            Assert.Equal(company.Name, resultCompany.Name);
        }

        [Fact]
        public void GivenPost_WhenCompanyIsNull_ThenReturnBadRequest()
        {
            //arrange
            var company = _fixture.Create<Company>();
            _mockRepository
                .Setup(a => a.Insert(It.IsAny<Company>()))
                .Returns(company)
                .Verifiable();

            //act
            var result = _controller.Post(null);

            //assert
            _mockRepository.Verify(a => a.Insert(It.IsAny<Company>()), Times.Never());
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async void GivenPost_WhenModelStateIsInvalid_ThenReturnBadRequest()
        {
            //arrange
            var companyPayload = JsonConvert.SerializeObject(new Company
            {
                CompanyId = Guid.NewGuid(),
                Isin = "001234567890",
                Name = "Company Name",
                Website = "website-invalid-address"
            });
            _outputHelper.WriteLine("Company value:\n\t {0}", companyPayload);
            HttpRequestMessage postRequest = new HttpRequestMessage(HttpMethod.Post, "api/company")
            {
                Content = new StringContent(companyPayload, Encoding.UTF8, "application/json"),
            };
            HttpResponseMessage response;

            //act (in memory test with TestServer)
            using (var client = _testServer.CreateClient())
            {
                response = await client.SendAsync(postRequest);
                _outputHelper.WriteLine("Response value:\n\t {0}", await response.Content.ReadAsStringAsync());
            }

            //assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        public void Dispose()
        {
            _testServer?.Dispose();
        }
    }
}
