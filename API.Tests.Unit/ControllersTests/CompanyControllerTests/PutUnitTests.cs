using API.Controllers;
using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Model.Company;
using Moq;
using System;
using Xunit;

namespace API.Tests.Unit.ControllersTests.CompanyControllerTests
{
    public class PutUnitTests
    {
        private readonly Fixture _fixture;
        private readonly Mock<ICompanyRepository> _mockRepository;
        private readonly CompanyController _controller;

        public PutUnitTests()
        {
            _fixture = new AutoFixture.Fixture();
            _mockRepository = new Mock<ICompanyRepository>();
            _controller = new CompanyController(_mockRepository.Object);
        }

        [Fact]
        public void GivenPut_WhenCompanyIsValid_ThenReturnAccepted()
        {
            //arrange
            var company = _fixture.Create<Company>();
            _mockRepository
                .Setup(a => a.Update(It.IsAny<Guid>(), It.IsAny<Company>()))
                .Returns(company)
                .Verifiable();

            //act
            var result = _controller.Put(company.CompanyId, company);

            //assert
            _mockRepository
                .Verify(a => a.Update(
                    It.Is<Guid>(c => c.Equals(company.CompanyId)),
                    It.Is<Company>(c => c.Name == company.Name)),
                    Times.Once());
            Assert.IsType<AcceptedAtActionResult>(result);
            var acceptedResult = result as AcceptedAtActionResult;
            Assert.NotNull(acceptedResult);
            Assert.IsType<Company>(acceptedResult.Value);
            var resultCompany = acceptedResult.Value as Company;
            Assert.NotNull(resultCompany);
            Assert.Equal(company.CompanyId, resultCompany.CompanyId);
            Assert.Equal(company.Name, resultCompany.Name);
        }

        [Fact]
        public void GivenPut_WhenCompanyIsNull_ThenReturnBadRequest()
        {
            //arrange
            var company = _fixture.Create<Company>();
            _mockRepository
                .Setup(a => a.Update(It.IsAny<Guid>(), It.IsAny<Company>()))
                .Returns(company)
                .Verifiable();

            //act
            var result = _controller.Put(company.CompanyId, null);

            //assert
            _mockRepository.Verify(a => a.Update(It.IsAny<Guid>(), It.IsAny<Company>()), Times.Never());
            Assert.IsType<BadRequestResult>(result);
        }
    }
}
