using API.Controllers;
using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Model.Company;
using Moq;
using Xunit;

namespace API.Tests.Unit.ControllersTests.CompanyControllerTests
{
    public class GetByIsinUnitTests
    {
        private readonly Fixture _fixture;
        private readonly Mock<ICompanyRepository> _mockRepository;
        private readonly CompanyController _controller;

        public GetByIsinUnitTests()
        {
            _fixture = new AutoFixture.Fixture();
            _mockRepository = new Mock<ICompanyRepository>();
            _controller = new CompanyController(_mockRepository.Object);
        }

        [Fact]
        public void GivenGetByIsin_WhenDbIsEmpty_ThenReturnNotFound()
        {
            //arrange
            string isin = _fixture.Create<string>();

            //act
            var result = _controller.GetByIsin(isin);

            //assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void GivenGetByIsin_WhenCompanyDoesntExist_ThenReturnNotFound()
        {
            //arrange
            var searchIsin = _fixture.Create<string>();
            var existingCompany = _fixture.Create<Company>();

            _mockRepository
                .Setup(a => a.SelectByIsin(It.Is<string>(b => b == existingCompany.Isin)))
                .Returns(existingCompany);

            //act
            var result = _controller.GetByIsin(searchIsin);

            //assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void GivenGetByIsin_WhenCompanyExists_ThenReturnOk()
        {
            //arrange
            var existingCompany = _fixture.Create<Company>();

            _mockRepository
                .Setup(a => a.SelectByIsin(It.Is<string>(b => b == existingCompany.Isin)))
                .Returns(existingCompany);

            //act
            var result = _controller.GetByIsin(existingCompany.Isin);

            //assert
            Assert.IsType<OkObjectResult>(result);
        }
    }
}
