using AutoFixture;
using Microsoft.EntityFrameworkCore;
using Model.Company;
using System.Linq;
using Xunit;

namespace Model.Tests.Unit.CompanyTests.CompanyRepositoryTests
{
    public class SelectByIsinUnitTests
    {
        private readonly Fixture _fixture;

        public SelectByIsinUnitTests()
        {
            _fixture = new AutoFixture.Fixture();
        }

        [Fact]
        public void GivenSelectByIsin_WhenDbIsEmpty_ThenReturnNull()
        {
            //arrange
            var searchIsin = _fixture.Create<string>();
            var optionsBuilder = new DbContextOptionsBuilder()
                .UseInMemoryDatabase(databaseName: _fixture.Create<string>())
                .Options;
            var mockContext = new ApplicationDbContext(optionsBuilder);
            var repository = new CompanyRepository(mockContext);

            //act
            var result = repository.SelectByIsin(searchIsin);

            //assert
            Assert.Null(result);
        }

        [Fact]
        public void GivenSelectByIsin_WhenDataExists_ThenReturnData()
        {
            //arrange
            var optionsBuilder = new DbContextOptionsBuilder()
                .UseInMemoryDatabase(databaseName: _fixture.Create<string>())
                .Options;
            var mockContext = new ApplicationDbContext(optionsBuilder);
            var repository = new CompanyRepository(mockContext);
            var companies = _fixture.CreateMany<Company.Company>().ToList();
            foreach (var company in companies)
            {
                mockContext.Company.Add(company);
            }
            mockContext.SaveChanges();

            //act
            var result = repository.SelectByIsin(companies[0].Isin);

            //assert
            Assert.NotNull(result);
            Assert.Equal(companies[0].CompanyId, result.CompanyId);
            Assert.Equal(companies[0].Name, result.Name);
        }

        [Fact]
        public void GivenSelectByIsin_WhenDataDoesntExist_ThenReturnNull()
        {
            //arrange
            var searchIsin = _fixture.Create<string>();
            var optionsBuilder = new DbContextOptionsBuilder()
                .UseInMemoryDatabase(databaseName: _fixture.Create<string>())
                .Options;
            var mockContext = new ApplicationDbContext(optionsBuilder);
            var repository = new CompanyRepository(mockContext);

            var companies = _fixture.CreateMany<Company.Company>().ToList();
            foreach (var company in companies)
            {
                mockContext.Company.Add(company);
            }
            mockContext.SaveChanges();

            //act
            var result = repository.SelectByIsin(searchIsin);

            //assert
            Assert.Null(result);
        }
    }
}
