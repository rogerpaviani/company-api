using AutoFixture;
using Microsoft.EntityFrameworkCore;
using Model.Company;
using System.Linq;
using Xunit;

namespace Model.Tests.Unit.CompanyTests.CompanyRepositoryTests
{
    public class SelectAllUnitTests
    {
        private readonly Fixture _fixture;

        public SelectAllUnitTests()
        {
            _fixture = new AutoFixture.Fixture();
        }

        [Fact]
        public void GivenSelectAll_WhenDbIsEmpty_ThenReturnEmpty()
        {
            //arrange
            var optionsBuilder = new DbContextOptionsBuilder()
                .UseInMemoryDatabase(databaseName: _fixture.Create<string>())
                .Options;
            var mockContext = new ApplicationDbContext(optionsBuilder);
            var repository = new CompanyRepository(mockContext);

            //act
            var result = repository.SelectAll();

            //assert
            Assert.Empty(result);
        }

        [Fact]
        public void GivenSelectAll_WhenDataExists_ThenReturnData()
        {
            //arrange
            var optionsBuilder = new DbContextOptionsBuilder()
                .UseInMemoryDatabase(databaseName: _fixture.Create<string>())
                .Options;
            var mockContext = new ApplicationDbContext(optionsBuilder);
            var repository = new CompanyRepository(mockContext);
            var companies = _fixture.CreateMany<Company.Company>().ToList();
            foreach (var company in companies)
            {
                mockContext.Company.Add(company);
            }
            mockContext.SaveChanges();

            //act
            var result = repository.SelectAll();

            //assert
            Assert.NotNull(result);
            Assert.Equal(companies.Count, result.Length);
        }
    }
}
