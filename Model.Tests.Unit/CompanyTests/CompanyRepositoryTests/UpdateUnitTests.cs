using AutoFixture;
using Microsoft.EntityFrameworkCore;
using Model.Company;
using System.Linq;
using Xunit;

namespace Model.Tests.Unit.CompanyTests.CompanyRepositoryTests
{
    public class UpdateUnitTests
    {
        private readonly Fixture _fixture;

        public UpdateUnitTests()
        {
            _fixture = new AutoFixture.Fixture();
        }

        [Fact]
        public void GivenUpdate_ThenCompanyIsUpdateInTheContext()
        {
            //arrange
            var optionsBuilder = new DbContextOptionsBuilder()
                .UseInMemoryDatabase(databaseName: _fixture.Create<string>())
                .Options;
            var mockContext = new ApplicationDbContext(optionsBuilder);
            var repository = new CompanyRepository(mockContext);
            var companies = _fixture.CreateMany<Company.Company>().ToList();
            foreach (var company in companies)
            {
                mockContext.Company.Add(company);
            }
            mockContext.SaveChanges();
            var updatingCompany = _fixture
                .Build<Company.Company>()
                .With(a => a.CompanyId, companies[0].CompanyId)
                .Create();

            //act
            var result = repository.Update(updatingCompany.CompanyId, updatingCompany);

            //assert
            Assert.NotNull(result);
            Assert.Equal(result.CompanyId, updatingCompany.CompanyId);
            Assert.Equal(result.Name, updatingCompany.Name);
        }
    }
}
