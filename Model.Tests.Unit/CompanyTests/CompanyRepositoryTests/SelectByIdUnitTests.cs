using AutoFixture;
using Microsoft.EntityFrameworkCore;
using Model.Company;
using System;
using System.Linq;
using Xunit;

namespace Model.Tests.Unit.CompanyTests.CompanyRepositoryTests
{
    public class SelectByIdUnitTests
    {
        private readonly Fixture _fixture;

        public SelectByIdUnitTests()
        {
            _fixture = new AutoFixture.Fixture();
        }

        [Fact]
        public void GivenSelectById_WhenDbIsEmpty_ThenReturnNull()
        {
            //arrange
            var searchCompanyId = Guid.NewGuid();
            var optionsBuilder = new DbContextOptionsBuilder()
                .UseInMemoryDatabase(databaseName: _fixture.Create<string>())
                .Options;
            var mockContext = new ApplicationDbContext(optionsBuilder);
            var repository = new CompanyRepository(mockContext);

            //act
            var result = repository.SelectById(searchCompanyId);

            //assert
            Assert.Null(result);
        }

        [Fact]
        public void GivenSelectById_WhenDataExists_ThenReturnData()
        {
            //arrange
            var optionsBuilder = new DbContextOptionsBuilder()
                .UseInMemoryDatabase(databaseName: _fixture.Create<string>())
                .Options;
            var mockContext = new ApplicationDbContext(optionsBuilder);
            var repository = new CompanyRepository(mockContext);
            var companies = _fixture.CreateMany<Company.Company>().ToList();
            foreach (var company in companies)
            {
                mockContext.Company.Add(company);
            }
            mockContext.SaveChanges();

            //act
            var result = repository.SelectById(companies[0].CompanyId);

            //assert
            Assert.NotNull(result);
            Assert.Equal(companies[0].CompanyId, result.CompanyId);
            Assert.Equal(companies[0].Name, result.Name);
        }

        [Fact]
        public void GivenSelectById_WhenDataDoesntExist_ThenReturnNull()
        {
            //arrange
            var searchCompanyId = Guid.NewGuid();
            var optionsBuilder = new DbContextOptionsBuilder()
                .UseInMemoryDatabase(databaseName: _fixture.Create<string>())
                .Options;
            var mockContext = new ApplicationDbContext(optionsBuilder);
            var repository = new CompanyRepository(mockContext);

            var companies = _fixture.CreateMany<Company.Company>().ToList();
            foreach (var company in companies)
            {
                mockContext.Company.Add(company);
            }
            mockContext.SaveChanges();

            //act
            var result = repository.SelectById(searchCompanyId);

            //assert
            Assert.Null(result);
        }
    }
}
