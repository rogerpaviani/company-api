using AutoFixture;
using Microsoft.EntityFrameworkCore;
using Model.Company;
using System.Linq;
using Xunit;

namespace Model.Tests.Unit.CompanyTests.CompanyRepositoryTests
{
    public class InsertUnitTests
    {
        private readonly Fixture _fixture;

        public InsertUnitTests()
        {
            _fixture = new AutoFixture.Fixture();
        }

        [Fact]
        public void GivenInsert_ThenCompanyIsAddedToContext()
        {
            //arrange
            var optionsBuilder = new DbContextOptionsBuilder()
                .UseInMemoryDatabase(databaseName: _fixture.Create<string>())
                .Options;
            var mockContext = new ApplicationDbContext(optionsBuilder);
            var repository = new CompanyRepository(mockContext);
            var companies = _fixture.CreateMany<Company.Company>().ToList();

            //act
            foreach (var company in companies)
            {
                repository.Insert(company);
            }

            //assert
            Assert.NotEmpty(mockContext.Company);
            Assert.Equal(companies.Count, mockContext.Company.ToArray().Length);
        }
    }
}
