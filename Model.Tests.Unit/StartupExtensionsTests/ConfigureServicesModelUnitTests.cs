﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Model.Company;
using System;
using System.Collections.Generic;
using Xunit;

namespace Model.Tests.Unit.StartupExtensionsTests
{
    public class ConfigureServicesModelUnitTests
    {
        [Theory]
        [InlineData(typeof(CompanyRepository), typeof(ICompanyRepository))]
        [InlineData(typeof(ApplicationDbContext), typeof(ApplicationDbContext))]
        public void GivenConfigure_ThenServicesAreReturned(Type expectedType, Type type)
        {
            //arrange
            var builder = new ConfigurationBuilder()
                .AddInMemoryCollection(new Dictionary<string, string> {
                    { "ConnectionStrings:CompanyDB", "server=localhost;database=db;userid=root;pwd=;port=3306;" }
                });
            var configuration = builder.Build();
            var serviceCollection = new ServiceCollection();
            serviceCollection.ConfigureServicesModel(configuration);
            var serviceProvider = serviceCollection.BuildServiceProvider();

            //act
            var result = serviceProvider.GetRequiredService(type);

            //assert
            Assert.IsType(expectedType, result);
        }
    }
}
